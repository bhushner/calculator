function cls() {
  document.getElementById("display-in").value = "";
  var myList = document.getElementById("myresult");
  myList.innerHTML = "";
}
function useHistory() {
  var expr1 = this.innerHTML.split("=");
  document.getElementById("display-in").value = expr1[0];
}
function equals() {
  var expr = document.getElementById("display-in").value;
  var result = eval(expr);
  var history = expr + " = " + result;
  var node = document.createElement("LI");
  node.value = expr;
  var textnode = document.createTextNode(history);
  node.appendChild(textnode);
  node.onclick = useHistory;
  node.id = "lists";
  document.getElementById("myresult").appendChild(node);
  document.getElementById("display-in").value = result;
}
