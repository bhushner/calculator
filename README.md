# A Simple Calculator
A simple calculator using **HTML, CSS, JavaScript**

## Technology
HTML, CSS, JAVASCRIPT

### Operations
**Basic Operations**
- [x] Addition ( operand1 + operand2 )
- [x] Substraction ( operand1 - operand2 )
- [x] Multiplication ( operand1 * operand2 )
- [x] Division ( operand1 / operand2 )
- [ ] ...
- [ ] ...

**Advanced Operations**
- [ ] ...
- [ ] ...
- [ ] ...
